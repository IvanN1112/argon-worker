<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta
            name="description"
            content="Start your development with a Design System for Bootstrap 4."
        />
        <meta name="author" content="Creative Tim" />
        <title>Argon Design System - Free Design System for Bootstrap 4</title>
        <!-- Favicon -->
        <link
            href="../assets/img/brand/favicon.png"
            rel="icon"
            type="image/png"
        />
        <!-- Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
            rel="stylesheet"
        />
        <!-- Icons -->
        <link href="../assets/vendor/nucleo/css/nucleo.css" rel="stylesheet" />
        <link
            href="../assets/vendor/font-awesome/css/font-awesome.min.css"
            rel="stylesheet"
        />
        <!-- Argon CSS -->
        <link
            type="text/css"
            href="../assets/css/argon.css?v=1.0.1"
            rel="stylesheet"
        />
        {{-- Custom styles --}}
        <link rel="stylesheet" href={{asset('public/style.css')}}>
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-default">
            <div class="container">
                <a class="navbar-brand" href="#">Default Color</a>
                <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbar-default"
                    aria-controls="navbar-default"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-default">
                    <div class="navbar-collapse-header">
                        <div class="row">
                            <div class="col-6 collapse-brand">
                                <a href="javascript:void(0)">
                                    <img
                                        src="../../assets/img/brand/blue.png"
                                    />
                                </a>
                            </div>
                            <div class="col-6 collapse-close">
                                <button
                                    type="button"
                                    class="navbar-toggler"
                                    data-toggle="collapse"
                                    data-target="#navbar-default"
                                    aria-controls="navbar-default"
                                    aria-expanded="false"
                                    aria-label="Toggle navigation"
                                >
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="#">
                                <i class="ni ni-favourite-28"></i>
                                <span class="nav-link-inner--text d-lg-none"
                                    >Discover</span
                                >
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="#">
                                <i class="ni ni-notification-70"></i>
                                <span class="nav-link-inner--text d-lg-none"
                                    >Profile</span
                                >
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a
                                class="nav-link nav-link-icon"
                                href="#"
                                id="navbar-default_dropdown_1"
                                role="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <i class="ni ni-settings-gear-65"></i>
                                <span class="nav-link-inner--text d-lg-none"
                                    >Settings</span
                                >
                            </a>
                            <div
                                class="dropdown-menu dropdown-menu-right"
                                aria-labelledby="navbar-default_dropdown_1"
                            >
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#"
                                    >Another action</a
                                >
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#"
                                    >Something else here</a
                                >
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container users mb-4">
            <div class="row">
                @foreach($users as $user)
                <div class="card  col-sm-5 col-md-3 offset-sm-1 mb-2 mt-4" style="width: 18rem;border-width:0.4rem;">
                <img class="card-img-top w-75 mx-auto" src="{{$user->avatar_url}}" alt="image not found, please contact us if this keeps happening">
                <div class="card-body text-center">
                    <h5 class="card-title">{{$user->name}}</h5>
                    <p class="card-text">{{$user->text}}</p>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Primary</button>
                    <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                    </div>
               </div><!-- /btn-group -->
                </div>
                </div>
                @endforeach
            </div>
        </div>

        {{ $users->links() }}

        <footer class="footer has-cards">
            <div class="container container-lg"></div>
            <div class="container">
                <div class="row row-grid align-items-center my-md">
                    <div class="col-lg-6">
                        <h3 class="text-primary font-weight-light mb-2">
                            Thank you for supporting us!
                        </h3>
                        <h4 class="mb-0 font-weight-light">
                            Let's get in touch on any of these platforms.
                        </h4>
                    </div>
                    <div class="col-lg-6 text-lg-center btn-wrapper">
                        <a
                            target="_blank"
                            href="https://twitter.com/creativetim"
                            class="
                                btn
                                btn-neutral
                                btn-icon-only
                                btn-twitter
                                btn-round
                                btn-lg
                            "
                            data-toggle="tooltip"
                            data-original-title="Follow us"
                        >
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a
                            target="_blank"
                            href="https://www.facebook.com/creativetim"
                            class="
                                btn
                                btn-neutral
                                btn-icon-only
                                btn-facebook
                                btn-round
                                btn-lg
                            "
                            data-toggle="tooltip"
                            data-original-title="Like us"
                        >
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <a
                            target="_blank"
                            href="https://dribbble.com/creativetim"
                            class="
                                btn
                                btn-neutral
                                btn-icon-only
                                btn-dribbble
                                btn-lg
                                btn-round
                            "
                            data-toggle="tooltip"
                            data-original-title="Follow us"
                        >
                            <i class="fa fa-dribbble"></i>
                        </a>
                        <a
                            target="_blank"
                            href="https://github.com/creativetimofficial"
                            class="
                                btn
                                btn-neutral
                                btn-icon-only
                                btn-github
                                btn-round
                                btn-lg
                            "
                            data-toggle="tooltip"
                            data-original-title="Star on Github"
                        >
                            <i class="fa fa-github"></i>
                        </a>
                    </div>
                </div>
                <hr />
                <div class="row align-items-center justify-content-md-between">
                    <div class="col-md-6">
                        <div class="copyright">
                            &copy; 2018
                            <a
                                href="https://www.creative-tim.com"
                                target="_blank"
                                >Creative Tim</a
                            >.
                        </div>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav nav-footer justify-content-end">
                            <li class="nav-item">
                                <a
                                    href="https://www.creative-tim.com"
                                    class="nav-link"
                                    target="_blank"
                                    >Creative Tim</a
                                >
                            </li>
                            <li class="nav-item">
                                <a
                                    href="https://www.creative-tim.com/presentation"
                                    class="nav-link"
                                    target="_blank"
                                    >About Us</a
                                >
                            </li>
                            <li class="nav-item">
                                <a
                                    href="http://blog.creative-tim.com"
                                    class="nav-link"
                                    target="_blank"
                                    >Blog</a
                                >
                            </li>
                            <li class="nav-item">
                                <a
                                    href="https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md"
                                    class="nav-link"
                                    target="_blank"
                                    >MIT License</a
                                >
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Core -->
        <script src="../assets/vendor/jquery/jquery.min.js"></script>
        <script src="../assets/vendor/popper/popper.min.js"></script>
        <script src="../assets/vendor/bootstrap/bootstrap.min.js"></script>
        <script src="../assets/vendor/headroom/headroom.min.js"></script>
        <!-- Argon JS -->
        <script src="../assets/js/argon.js?v=1.0.1"></script>
    </body>
</html>
