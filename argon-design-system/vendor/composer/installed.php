<?php return array(
    'root' => array(
        'pretty_version' => 'v1.0.1',
        'version' => '1.0.1.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'creativetimofficial/argon-design-system',
        'dev' => true,
    ),
    'versions' => array(
        'creativetimofficial/argon-design-system' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
