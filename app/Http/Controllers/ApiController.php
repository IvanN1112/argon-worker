<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ApiController extends Controller
{
    public function test()
    {
        $responseUser = Http::get('https://random-data-api.com/api/users/random_user');
        $responseText = Http::get('https://random-data-api.com/api/lorem_ipsum/random_lorem_ipsum')['very_long_sentence'];
        $user = ['name' => $responseUser['first_name'] . ' ' . $responseUser['last_name'], 'email' => $responseUser['email'], 'password' => bcrypt($responseUser['password']), 'avatar_url' => $responseUser['avatar'], 'text' => $responseText];
        User::create($user);
    }
}
