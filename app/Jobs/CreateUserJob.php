<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Http;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CreateUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $responseUser = Http::get('https://random-data-api.com/api/users/random_user');
        $responseText = Http::get('https://random-data-api.com/api/lorem_ipsum/random_lorem_ipsum')['long_sentence'];
        $user = ['name' => $responseUser['first_name'] . ' ' . $responseUser['last_name'], 'email' => $responseUser['email'], 'password' => bcrypt($responseUser['password']), 'avatar_url' => $responseUser['avatar'], 'text' => $responseText];
        User::create($user);
    }
}
